package com.armanjb.tdp;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by ArmanJB on 08/04/2016.
 */
public class Adapter extends PagerAdapter{
    @Override
    public int getCount() {
        return 6;
    }

    public Object instantiateItem(View collection, int position){
        LayoutInflater inflater = (LayoutInflater) collection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int resId = 0;
        switch (position){
            case 0:
                resId = R.layout.page1;
                break;
            case 1:
                resId = R.layout.page2;
                break;
            case 2:
                resId = R.layout.page3;
                break;
            case 3:
                resId = R.layout.page4;
                break;
            case 4:
                resId = R.layout.page5;
                break;
            case 5:
                resId = R.layout.page6;
                break;
        }
        View view = inflater.inflate(resId, null);
        ((ViewPager)collection).addView(view, 0);
        return view;
    }

    @Override
    public  void destroyItem(View view, int i, Object o){
        ((ViewPager)view).removeView((View)o);
    }
    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view==((View)o);
    }
    @Override
    public Parcelable saveState(){
        return null;
    }
}
